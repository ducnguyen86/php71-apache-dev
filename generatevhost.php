<?php
var_dump($argv);
if (empty($argv[1])) {
  echo "please input the path to sites directory";
}
$directories = scandir($argv[1]);
$directories[] = $argv[1];
if (is_dir($argv[1])) {
  $vhosts = [];
  foreach ($directories as $name) {
    $dir = $argv[1] . '/'. $name;
    $dir = str_replace('//', '/', $dir);
    if (is_dir($dir) && strpos($name, '.') === false) {
      $public_dir = getPublicDir($dir);
      $vhost = ['name' => $name, 'dir' => $dir, 'public_dir' => $public_dir];
      $vhosts = $vhost;
      createVhost($vhost);
    }
  }
  passthru("service apache2 reload");
}

/**
 * create apache vhost.
 */
function createVhost($vhost) {
  $name = $vhost['name'];
  $dir = $vhost['dir'];
  $public_dir = $vhost['public_dir'];
  echo "Create $name.test virtualhost";
  echo " with root directory: " . $dir;
  echo " and public_html directory: " . $public_dir. PHP_EOL;
  exec("/usr/local/bin/virtualhost delete $name.test");
  passthru("/usr/local/bin/virtualhost create $name.test $public_dir");
}
/**
 * guess the public directory for apache
 *
 */
function getPublicDir($dir) {
  if (!is_dir($dir))
    return false;
  $public = $dir;
  $funcs = ['is_drupal8_site', 'is_laravel5_site'];

  foreach ($funcs as $func) {
    $ret = call_user_func($func, $dir);
    if ($ret !== false) {
      $public = $ret;
      break;
    }
  }
  $public = str_replace('//', '/', $public);
  return $public;
}

function is_drupal8_site($dir) {
  if (file_exists($dir.'/web') && file_exists($dir.'/web/sites/default/default.services.yml')) {
    return $dir.'/web';
  }
  else {
    return false;
  }
}
function is_laravel5_site($dir) {
  if (file_exists($dir.'/public') && file_exists($dir.'/artisan')) {
    return $dir.'/public';
  }
  else {
    return false;
  }
}
?>