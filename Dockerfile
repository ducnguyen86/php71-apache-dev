FROM ubuntu:14.04
MAINTAINER Duc Nguyen <minhduclol@gmail.com>
# Set correct environment variables.
ENV HOME /root
ENV DEBIAN_FRONTEND noninteractive
ENV INITRD No

# Our user in the container
USER root
WORKDIR /var/www/html/

RUN apt-get update -y && apt-get install -y software-properties-common \
  language-pack-en-base \
  git curl wget apache2
RUN a2enmod rewrite
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8
#add latest nodejs repo
RUN curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
#https://lornajane.net/posts/2016/php-7-0-and-5-6-on-ubuntu
RUN yes | add-apt-repository ppa:ondrej/php
RUN apt-get update -y
RUN apt-get install -y php7.1 php7.1-mbstring php7.1-mysql php7.1-xml php7.1-cgi php7.1-fpm php7.1-common php7.1-curl php7.1-gd php7.1-tidy php7.1-json php7.1-bcmath php7.1-mbstring php7.1-soap php7.1-zip php7.1-bz2 php7.1-dev \
 php-xml php-zip php-pear
# Set Default PHP Version 5.6
RUN update-alternatives --set php /usr/bin/php7.1
RUN apt-get install -y mysql-client memcached php-memcached
RUN a2enmod proxy_fcgi setenvif
RUN a2enconf php7.1-fpm
#check php version
RUN php -v
RUN pear channel-update pear.php.net
RUN pecl config-set preferred_state beta

RUN yes | pecl install xdebug
RUN echo "zend_extension=$(find /usr/lib/php/ -name xdebug.so)" > /etc/php/7.1/mods-available/xdebug.ini \
    && echo "xdebug.remote_enable=on" >> /etc/php/7.1/mods-available/xdebug.ini \
    && echo "xdebug.remote_autostart=off" >> /etc/php/7.1/mods-available/xdebug.ini

#setup dev value
# 'Set max_execution_time = 300'
RUN find /etc/php/ -type f -name php.ini -exec sed -i 's/max_execution_time = 30/max_execution_time = 300/g' {} ";"
# 'Set memory_limit = 512M'
RUN find /etc/php/ -type f -name php.ini -exec sed -i -e 's/memory_limit = 128M/memory_limit = 512M/g' {} ";"
# 'Set display_errors = On'
RUN find /etc/php/ -type f -name php.ini -exec sed -i -e 's/display_errors = Off/display_errors = On/g' {} ";"
#install composer
RUN curl -s https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer
RUN chmod +x /usr/local/bin/composer
ENV PATH="$HOME/.composer/vendor/bin:$PATH"
RUN composer global require drush/drush:8.*
RUN composer global require "laravel/installer"
RUN mkdir /run/php && chown www-data:www-data /run/php
RUN echo "DirectoryIndex index.html index.php" >> /etc/apache2/apache2.conf
# set AllowOverride All to defaul host
RUN find /etc/apache2/ -type f -name apache2.conf -exec sed -i -e 's/AllowOverride All/AllowOverride All/g' {} ";"
RUN sed -i '/listen = \/run\/php\/php7.1-fpm.sock/alisten = [::]:9000' /etc/php/7.1/fpm/pool.d/www.conf
#RUN service php7.1-fpm restart
RUN apt-get install -y supervisor nano nodejs
RUN npm config set strict-ssl false
RUN npm install gulp-cli -g
COPY supervisor_app.conf /etc/supervisor/conf.d/devwebapp.conf
COPY 000-default.conf /etc/apache2/sites-enabled/000-default.conf
COPY src/ /var/www/html/
# Usage sudo sh /path/to/virtualhost.sh [create | delete] [domain] [optional host_dir]
COPY virtualhost.sh /usr/local/bin/virtualhost
RUN chmod +x /usr/local/bin/virtualhost
# Add cronjob to generate virtualhost every minute
COPY generatevhost.php /root/generatevhost.php
RUN wget -q -O - https://packagecloud.io/gpg.key | sudo apt-key add -
RUN echo "deb http://packages.blackfire.io/debian any main" | sudo tee /etc/apt/sources.list.d/blackfire.list
RUN apt-get update
RUN apt-get install blackfire-agent blackfire-php
#RUN service supervisord start
# Define mountable directories.
VOLUME ["/var/www/html/"]
EXPOSE 80 443 9000

CMD ["/usr/bin/supervisord"]
#ENTRYPOINT ["/usr/bin/supervisord", "-c /etc/supervisor/supervisord.conf"]
#ENTRYPOINT ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]